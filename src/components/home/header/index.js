import { Navbar, Container, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
export const HomeHeaderComponent = () => {
    return (<>
     <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#home">Navbar</Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#features">Features</Nav.Link>
                    <Nav.Link href="#pricing">Pricing</Nav.Link>

                    <NavLink className="nav-link" to="/admin/login">Login</NavLink>
                </Nav>
            </Container>
        </Navbar>
    </>);
}