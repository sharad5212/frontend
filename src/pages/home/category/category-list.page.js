import { useParams, useSearchParams } from "react-router-dom"
import { HomeHeaderComponent } from "../../../components/home/header"

export const CategoryList = () => {
    let params = useParams();
    
    // let [query] = useSearchParams();

    //location.search

    // console.log(query.get('test'));
    // console.log(query.get('newkey'));
    return (
        <>
            <HomeHeaderComponent />

            <p>Params: {params.id}</p>
        </>
    )
}